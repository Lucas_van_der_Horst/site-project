import minimax from './minimax.js';
import model from './model.js';

function draw_GameState(game_state) {
    const c = document.getElementById("myCanvas");
    const ctx = c.getContext("2d");
    let token_width = c.width / game_state.board.length;
    let token_height = c.height / game_state.board[0].length;

    ctx.fillStyle = "#3333FF";
    ctx.fillRect(0, 0, c.width, c.height);

    for(let x=0; x<game_state.board.length; x++) {
        for(let y=0; y<game_state.board[0].length; y++) {
            switch(game_state.board[x][y]) {
                case 1:
                    ctx.fillStyle = "#FFDD00";
                    break;
                case 2:
                    ctx.fillStyle = "#EE0000";
                    break;
                default:
                    ctx.fillStyle = "#21252B";
            }

            let draw_x = x*token_width + token_width/2
            let draw_y = y*token_height + token_height/2
            ctx.beginPath();
            ctx.ellipse(draw_x, draw_y, token_width/2.5, token_height/2.5, 0, 0, Math.PI*2);
            ctx.fill();
        }
    }
}

function real_action(game_state, action) {
    model.apply_action(game_state, action);
    draw_GameState(game_state);

    let valid_actions_temp = model.valid_actions(game_state);
    for (let i = 0; i < game_state.board.length; i++) {
        document.getElementById("slot" + i).disabled = !valid_actions_temp.includes(i);
    }

    if(game_state.winner != null) {
        for(let i=0; i<game_state.board.length; i++) {
            document.getElementById("slot" + i).disabled = true;
        }
        alert("The " + ["yellow", "red"][game_state.winner-1] + " player won!");
    } else if(game_state.turn === game_state.computer) {
        real_action(game_state, minimax.choose_action(game_state, 6));
    }
}

let main_state = model.main_state;
draw_GameState(main_state);

let button_holder = document.getElementById("slotButtons");
for(let i=0; i<main_state.board.length; i++) {
    let button = document.createElement("BUTTON");
    button.id = "slot" + i;
    button.onclick = function () {real_action(main_state, i)};
    button.className = "button"
    button.textContent = "Button " + (i+1);

    button_holder.appendChild(button)
}