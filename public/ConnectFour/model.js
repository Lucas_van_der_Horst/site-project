function generate_board(width, height) {
    let board = new Array(width);
    for(let x=0; x<width; x++) {
        board[x] = new Array(height);
        for(let y=0; y<height; y++) {
            board[x][y] = 0;
        }
    }
    return board;
}

function apply_action(game_state, action) {
    let look = 0;
    while(game_state.board[action][look] === 0 && look < game_state.board[0].length){
        look++;
    }
    game_state.board[action][look-1] = game_state.turn;
    game_state.turn = [2, 1][game_state.turn-1];
    check_winner(game_state, action, look-1);
}


function valid_actions(game_state) {
    let valid_actions = [];
    const l = game_state.board.length;
    for(let i=0; i < l; i++) {
        const a = Math.ceil(i+l/2) % l;
        if(game_state.board[a][0] === 0) {
            valid_actions.push(a);
        }
    }
    return valid_actions;
}

function check_winner(game_state, from_x, from_y) {
    if(game_state.winner == null) {
        for(let determinants of [
            [function (i) {return from_x+i}, function () {return from_y}],
            [function () {return from_x}, function (i) {return from_y+i}],
            [function (i) {return from_x+i}, function (i) {return from_y+i}],
            [function (i) {return from_x+i}, function (i) {return from_y-i}],
        ]){
            let line = [];
            for(let i=-(game_state.length_to_win-1); i<game_state.length_to_win; i++) {
                let check_x = determinants[0](i);
                let check_y = determinants[1](i);
                if(check_x >= 0 && check_x < game_state.board.length && check_y >= 0 && check_y < game_state.board[0].length) {
                    line.push(game_state.board[check_x][check_y]);
                } else {
                    line.push(undefined);
                }
            }
            for(let i=0; i<game_state.length_to_win; i++) {
                switch (line.slice(i, i+game_state.length_to_win).toString()) {
                    case "1,".repeat(game_state.length_to_win-1) + "1":
                        game_state.winner = 1;
                        break;
                    case "2,".repeat(game_state.length_to_win-1) + "2":
                        game_state.winner = 2;
                        break;
                }
            }
        }
    }
}

let main_state = {
    board: generate_board(7, 6),
    turn: 1,
    computer: 2,
    length_to_win: 4,
    winner: undefined,
}

function clone_game_state(game_state) {
    const len = game_state.board.length;
    let new_board = new Array(len);
    for(let i=0; i<len; i++) {
        new_board[i] = game_state.board[i].slice(0);
    }
    return {
        board: new_board,
        turn: game_state.turn,
        computer: game_state.computer,
        length_to_win: game_state.length_to_win,
        winner: game_state.winner,
    }
}

export default {generate_board, apply_action, valid_actions, check_winner, main_state, clone_game_state}