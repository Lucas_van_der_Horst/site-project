import model from './model.js';

function best_choice(game_state, max_depth, depth) {
    if (game_state.winner != null) {
        return [-(10000-depth), undefined];
    }
    if (depth === max_depth) {
        return [0, undefined];
    }
    let valid_actions = model.valid_actions(game_state);
    if (valid_actions.length === 0) {
        return [0, undefined];
    }
    let best_score = -Infinity;
    let best_action = 0;
    for (const action of valid_actions) {
        let child = model.clone_game_state(game_state);
        model.apply_action(child, action);
        const score = -best_choice(child, max_depth, depth+1)[0];
        if(score >= best_score) {
            best_score = score;
            best_action = action;
        }
        if(depth === 0) {
            console.log("best action: "+ best_action + ". score: " + best_score);
        }
    }
    return [best_score, best_action];
}

function choose_action(game_state, max_depth) {
    return best_choice(game_state, max_depth, 0)[1];
}

export default {choose_action}