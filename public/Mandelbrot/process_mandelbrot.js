class Complex {
    constructor(real, imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }
}

function complex_to_string(complex) {
    return complex.real + "+" + complex.imaginary + "i";
}

function complex_sum(complex_a, complex_b) {
    return new Complex(complex_a.real + complex_b.real, complex_a.imaginary + complex_b.imaginary);
}

function complex_square(c) {
    return new Complex(c.real*c.real - c.imaginary*c.imaginary, 2*c.real*c.imaginary);
}

function complex_abs(c) {
    return Math.sqrt(c.real*c.real + c.imaginary*c.imaginary);
}


const iterations = 100;
const border = 2;

function process_point(x, y, trace=false) {
    const c = new Complex(x, y)
    let working = new Complex(x, y);
    let path = [Object.assign({}, working)];
    for (let i = 0; i < iterations; i++) {
        working = complex_sum(complex_square(working), c);
        path.push(Object.assign({}, working));
        if (complex_abs(working) > border) {
            if (trace) {
                return path;
            } else {
                return i + 1;
            }
        }
    }
    if (trace) {
        return path;
    } else {
        return 0;
    }
}


class Section {
    constructor(left, right, top, bottom, iteration) {
        this.left = left;
        this.right = right;
        this.top = top;
        this.bottom = bottom;
        this.sub_sections = [];

        if (iteration == null) {
            this.iteration = process_point((left+right)/2, (top+bottom)/2)
        } else {
            this.iteration = iteration
        }
        if(minIT == null || minIT > this.iteration) {
            minIT = this.iteration
            redraw = true;
        }
        if(maxIT == null || maxIT < this.iteration) {
            maxIT = this.iteration
            redraw = true;
        }
    }
}

let minIT = 0;
let maxIT = 100;
let redraw = false;
let palette = [{r:0,g:0,b:0},{r:255,g:200,b:0},{r:200,g:0,b:255},{r:0,g:255,b:255},{r:200,g:0,b:0}];

function convert_coordinate(value, dimension, to_system) {
    if (to_system === "mb") {
        if (dimension === "h") {
            return value/pc.width*(camera.right-camera.left)+camera.left;
        } else if (dimension === "v") {
            return value/pc.height*(camera.bottom-camera.top)+camera.top;
        }
    } else if (to_system === "dp") {
        if (dimension === "h") {
            return (value-camera.left) / (camera.right-camera.left) * pc.width;
        } else if (dimension === "v") {
            return (value-camera.top) / (camera.bottom-camera.top) * pc.height;
        }
    }
}

function linear_interpolate(color1, color2, ratio) {
    let r = Math.floor((color2.r - color1.r) * ratio + color1.r);
    let g = Math.floor((color2.g - color1.g) * ratio + color1.g);
    let b = Math.floor((color2.b - color1.b) * ratio + color1.b);
    return 'rgb(' + r + ',' + g + ',' + b + ')';
}

function coloring_function(iteration) {
    if (minIT === maxIT) {
        return palette[0];
    } else {
        let value = (iteration-minIT)/(maxIT-minIT)*(palette.length-2);
        let color1 = palette[Math.floor(value)];
        let color2 = palette[Math.floor(value) + 1];
        return linear_interpolate(color1, color2, value % 1);
    }
}

function draw_section(section, ctx) {
    if (
        (section.left>=camera.left&&section.left<camera.right) ||
        (camera.left>=section.left&&camera.left<section.right)
    ){
        const x = convert_coordinate(section.left, "h", "dp");
        const y = convert_coordinate(section.top, "v", "dp")
        const w = (section.right-section.left) / (camera.right-camera.left) * pc.width + 1;
        const h = (section.bottom-section.top) / (camera.bottom-camera.top) * pc.height + 1;
        if (Array.isArray(section.sub_sections) && section.sub_sections.length && w>=2 && h>=2) {
            for (let i in section.sub_sections) {
                draw_section(section.sub_sections[i], ctx);
            }
        } else {
            //ctx.fillStyle = '#'+Math.random().toString(16).substr(-6);
            ctx.fillStyle = coloring_function(section.iteration);
            ctx.fillRect(x, y, w, h);
        }
    }
}

function expand_section(s, x, y) {
    if (Array.isArray(s.sub_sections) && s.sub_sections.length) {
        for (let i in s.sub_sections) {
            let sub_s = s.sub_sections[i];
            if (x >= sub_s.left && x <= sub_s.right && y >= sub_s.top && y <= sub_s.bottom) {
                return expand_section(sub_s, x, y)
            }
        }
    } else {
        const width = (s.right - s.left) / 3;
        const height = (s.bottom - s.top) / 3;

        s.sub_sections.push(new Section(s.left, s.left+width, s.top, s.top+height));
        s.sub_sections.push(new Section(s.left+width, s.right-width, s.top, s.top+height));
        s.sub_sections.push(new Section(s.right-width, s.right, s.top, s.top+height));
        s.sub_sections.push(new Section(s.left, s.left+width, s.top+height, s.bottom-height));
        s.sub_sections.push(new Section(s.left+width, s.right-width, s.top+height, s.bottom-height, s.iteration));
        s.sub_sections.push(new Section(s.right-width, s.right, s.top+height, s.bottom-height));
        s.sub_sections.push(new Section(s.left, s.left+width, s.bottom-height, s.bottom));
        s.sub_sections.push(new Section(s.left+width, s.right-width, s.bottom-height, s.bottom));
        s.sub_sections.push(new Section(s.right-width, s.right, s.bottom-height, s.bottom));
        return s;
    }
}

let camera = {
    left: -2.2,
    right: 0.8,
    top: -1.3,
    bottom: 1.3
}

const pc = document.getElementById("mandelbrot_pixels_layer");
const ctx_pc = pc.getContext("2d");
const oc = document.getElementById("mandelbrot_overlay_layer");
const ctx_oc = oc.getContext("2d");
let root_section = new Section(camera.left, camera.right, camera.top, camera.bottom)
draw_section(root_section, ctx_pc)

let mouse_x = (camera.left + camera.right) / 2;
let mouse_y = (camera.top + camera.bottom) / 2;

addEventListener('mousemove', function (event) {
    mouse_x = convert_coordinate(event.clientX, "h", "mb");
    mouse_y = convert_coordinate(event.clientY, "v", "mb");

    draw_overlay(event);
}, false);

function randn_bm(min, max, skew) {
    let u = 0, v = 0;
    while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while(v === 0) v = Math.random();
    let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );

    num = num / 10.0 + 0.5; // Translate to 0 -> 1
    if (num > 1 || num < 0) num = randn_bm(min, max, skew); // resample between 0 and 1 if out of range
    num = Math.pow(num, skew); // Skew
    num *= max - min; // Stretch to fill range
    num += min; // offset to min
    return num;
}

setInterval(function() {
    const brush_size = (camera.right - camera.left) / 3
    let explore_x = mouse_x + randn_bm(-brush_size, brush_size, 1);
    let explore_y = mouse_y + randn_bm(-brush_size, brush_size, 1);
    let expanded = expand_section(root_section, explore_x, explore_y);
    if (redraw) {
        redraw = false;
        ctx_pc.fillStyle = "#000000";
        ctx_pc.fillRect(0, 0, pc.width, pc.height);
        draw_section(root_section, ctx_pc);
    } else {
        if (expanded != null) {
            draw_section(expanded, ctx_pc);
        }
    }
    if (false) {
        ctx_pc.fillStyle = 'rgba(225,225,225,0.1)';
        ctx_pc.fillRect(
            convert_coordinate(explore_x, "h", "dp"),
            convert_coordinate(explore_y, "v", "dp"),
            5, 5
        );
    }
}, 0);

let mouse_down_x = null;
let mouse_down_y = null;
let camera_history = [];

window.addEventListener('mousedown', function (event) {
    if(event.clientX <= pc.width && event.clientY <= pc.height) {
        mouse_down_x = event.clientX;
        mouse_down_y = event.clientY;
        //console.log("down: " + mouse_down_x + "  " + mouse_down_y);
    }
})

window.addEventListener('mouseup', function (event) {
    //console.log("up: " + event.clientX + "  " + event.clientY);
    let left = Math.min(mouse_down_x, event.clientX);
    let right = Math.max(mouse_down_x, event.clientX);
    let top = Math.min(mouse_down_y, event.clientY);
    let bottom = Math.max(mouse_down_y, event.clientY);

    if ((right-left) * (bottom-top) > 10 && event.clientX <= pc.width && event.clientY <= pc.height && mouse_down_x != null) {
        left = left/pc.width*(camera.right-camera.left)+camera.left;
        right = right/pc.width*(camera.right-camera.left)+camera.left;
        top = top/pc.height*(camera.bottom-camera.top)+camera.top;
        bottom = bottom/pc.height*(camera.bottom-camera.top)+camera.top;
        camera_history.push([camera.left, camera.right, camera.top, camera.bottom]);
        camera.left = left;
        camera.right = right;
        camera.top = top;
        camera.bottom = bottom;
        update_camera_insert();
        redraw = true;
        //minIT = null;
        //maxIT = null;
    }

    mouse_down_x = null;
    mouse_down_y = null;
    ctx_oc.clearRect(0, 0, oc.width, oc.height);
})

function camera_back() {
    if (camera_history.length > 0) {
        const select = camera_history[camera_history.length-1];
        camera.left = select[0];
        camera.right = select[1];
        camera.top = select[2];
        camera.bottom = select[3];
        update_camera_insert();
        redraw = true;
        camera_history.pop();
    }
}

function draw_overlay(event) {
    ctx_oc.clearRect(0, 0, oc.width, oc.height);

    //draw selection box
    if (mouse_down_x != null) {
        ctx_oc.beginPath();
        ctx_oc.lineWidth = 3;
        ctx_oc.strokeStyle = "white";
        ctx_oc.rect(mouse_down_x, mouse_down_y, event.clientX-mouse_down_x, event.clientY-mouse_down_y);
        ctx_oc.stroke();
        ctx_oc.lineWidth = 2;
        ctx_oc.strokeStyle = "black";
        ctx_oc.rect(mouse_down_x, mouse_down_y, event.clientX-mouse_down_x, event.clientY-mouse_down_y);
        ctx_oc.stroke();
    }

    //draw iteration path
    if (document.getElementById("IVC").checked) {
        ctx_oc.lineWidth = 1;
        ctx_oc.strokeStyle = "white";
        let last_x = convert_coordinate(0, "h", "dp");
        let last_y = convert_coordinate(0, "v", "dp");
        //console.log(process_point(mouse_x, mouse_y, true));
        const points = process_point(mouse_x, mouse_y, true);
        for (let i in points) {
            ctx_oc.beginPath();
            ctx_oc.moveTo(last_x, last_y);
            last_x = convert_coordinate(points[i].real, "h", "dp");
            last_y = convert_coordinate(points[i].imaginary, "v", "dp");
            ctx_oc.lineTo(last_x, last_y);
            ctx_oc.stroke();
        }
    }
}

function update_camera() {
    camera_history.push([camera.left, camera.right, camera.top, camera.bottom]);
    camera.left = parseFloat(document.getElementById("camera_left").value);
    camera.right = parseFloat(document.getElementById("camera_right").value);
    camera.top = parseFloat(document.getElementById("camera_top").value);
    camera.bottom = parseFloat(document.getElementById("camera_bottom").value);
    redraw = true;
}

function update_camera_insert() {
    document.getElementById("camera_left").value = camera.left;
    document.getElementById("camera_right").value = camera.right;
    document.getElementById("camera_top").value = camera.top;
    document.getElementById("camera_bottom").value = camera.bottom;
}

update_camera_insert();
