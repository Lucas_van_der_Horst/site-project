import torch
import matplotlib.pyplot as plt
from tqdm import tqdm
from visualize import visualize


def train(model, n_epochs, train_loader, test_loader, device, criterion, optimizer):
    best_score = float('inf')
    train_loss, test_loss = [], []
    for epoch in tqdm(range(n_epochs)):
        total_train_loss = 0
        for imgs, label in train_loader:
            imgs = imgs.to(device)
            output = model(imgs)
            loss = criterion(output, imgs)
            total_train_loss += loss.item() * imgs.size(0)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        tqdm.write(f"\nTraining loss {total_train_loss / len(train_loader.dataset)} on epoch {epoch}")
        if epoch % 4 == 0:
            total_test_loss = 0
            for imgs, label in test_loader:
                imgs = imgs.to(device)
                output = model(imgs)
                loss = criterion(output, imgs)
                total_test_loss += loss.item() * imgs.size(0)

            train_loss.append(total_train_loss / len(train_loader.dataset))
            test_loss.append(total_test_loss / len(test_loader.dataset))

            if total_test_loss < best_score:
                tqdm.write(f"\nNew best test loss of {test_loss[-1]}, saving model")
                best_score = total_test_loss
                torch.save(model, 'best_model.pth')

            visualize(model, test_loader, device, f"plots/plot_epoch{epoch}.png")

    plt.plot(train_loss, label='train')
    plt.plot(test_loss, label='test')
    plt.legend()
    plt.show()
