import torch
from torch import nn


class CustomUF(nn.Module):
    def __init__(self, output_shape):
        super(CustomUF, self).__init__()
        self.output_shape = output_shape
    
    def forward(self, x):
        return x.reshape((-1,) + self.output_shape)


class AutoEncoder(nn.Module):
    def __init__(self):
        super(AutoEncoder, self).__init__()
        self.encoder = nn.Sequential(
            nn.Conv2d(1, 16, 3, stride=3, padding=1),  # b, 16, 10, 10
            nn.ReLU(True),
            nn.MaxPool2d(2, stride=2),  # b, 16, 5, 5
            nn.Conv2d(16, 8, 3, stride=2, padding=1),  # b, 8, 3, 3
            nn.ReLU(True),
            nn.MaxPool2d(2, stride=1),  # b, 8, 2, 2
            nn.Flatten(start_dim=1),    # b, 32
            nn.Linear(32, 10),   # b, 10
            nn.ReLU(True),
            nn.Linear(10, 2),   # b, 2
            nn.Sigmoid(),
        )
        # self.decoder = nn.Sequential(
        #     nn.Linear(2, 10),
        #     nn.ReLU(True),
        #     nn.Linear(10, 32),
        #     nn.ReLU(True),
        #     CustomUF((8, 2, 2)),
        #     #nn.Unflatten(1, (8, 2, 2)),
        #     nn.ConvTranspose2d(8, 16, 3, stride=2),  # b, 16, 5, 5
        #     nn.ReLU(True),
        #     nn.ConvTranspose2d(16, 8, 5, stride=3, padding=1),  # b, 8, 15, 15
        #     nn.ReLU(True),
        #     nn.ConvTranspose2d(8, 1, 2, stride=2, padding=1),  # b, 1, 28, 28
        #     nn.Sigmoid()
        # )
        self.decoder = nn.Sequential(
            nn.Linear(2, 10),
            nn.ReLU(True),
            nn.Linear(10, 32),
            nn.ReLU(True),
            nn.Linear(32, 128),
            nn.ReLU(True),
            nn.Linear(128, 784),
            nn.Sigmoid(),
            CustomUF((1, 28, 28)),
        )

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x
