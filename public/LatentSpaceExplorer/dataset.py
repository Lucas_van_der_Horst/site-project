import torch
from torch import nn
import pandas as pd


class EMNIST(torch.utils.data.Dataset):
    def __init__(self, csv_path):
        self.data_csv = pd.read_csv(csv_path, header=None)
        self.unflatten = nn.Unflatten(-1, (28, 28))

    def __getitem__(self, i):
        row = self.data_csv.iloc[i]
        label, img = row[0], row[1:]
        img = self.unflatten(torch.tensor(img.values)).swapaxes(0, 1)
        return torch.unsqueeze(img, 0) / 255.0, label

    def __len__(self):
        return len(self.data_csv)
