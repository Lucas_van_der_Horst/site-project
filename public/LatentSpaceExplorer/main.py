import torch
import torchvision
from pathlib import Path
from model import AutoEncoder
from dataset import EMNIST
from train import train

batch_size_train, batch_size_test = 512, 1000
device = torch.device('cuda:0')
data_root = Path('archive')

train_set = EMNIST(data_root / 'emnist-digits-train.csv')
test_set = EMNIST(data_root / 'emnist-digits-test.csv')
train_loader = torch.utils.data.DataLoader(train_set, batch_size=batch_size_train, num_workers=12, shuffle=True)
test_loader = torch.utils.data.DataLoader(test_set, batch_size=batch_size_test, num_workers=12)

model = AutoEncoder().to(device)
criterion = torch.nn.MSELoss()
optimizer = torch.optim.Adam(model.parameters(), lr=1e-3, weight_decay=1e-5)
n_epochs = 1000

if __name__ == "__main__":
    train(model, n_epochs, train_loader, test_loader, device, criterion, optimizer)
