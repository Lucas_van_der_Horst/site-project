import pandas as pd
import seaborn as sns


def visualize(model, loader, device, file_path):
    df = {'label': [], 'encode0': [], 'encode1': []}

    for imgs, labels in loader:
        imgs = imgs.to(device)
        encoded = model.encoder(imgs).cpu()
        for label, enc in zip(labels, encoded):
            df['label'].append(str(label.item()))
            df['encode0'].append(enc[0].item())
            df['encode1'].append(enc[1].item())

    df = pd.DataFrame(df)
    plot = sns.scatterplot(data=df, x="encode0", y="encode1", hue="label")
    plot.get_figure().savefig(file_path)
